package io.redlink.ssix.pipeline.services;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import io.redlink.ssix.pipeline.nlp.api.SentimentClass;
import io.searchbox.core.CountResult;
import io.searchbox.core.SearchResult;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Some basic AnalyticsService integration tests
 *
 * @author Sergio Fernández
 */
public class AnalyticsServiceIT {

    private static final Logger log = LoggerFactory.getLogger(AnalyticsServiceIT.class);

    /*
    http://www.programcreek.com/java-api-examples/index.php?api=io.searchbox.client.JestResult
    @Test
    public void testSentimentAggregation() throws IOException {
        final JestClient client = mock(JestClient.class);
        final JestResult searchResult = new JestResult(new Gson());
        final String response = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("queries/sentiment_aggregation.json"));
        searchResult.setJsonString(response);
        searchResult.setJsonObject(new JsonParser().parse(response).getAsJsonObject());
        searchResult.setSucceeded(true);
        when(client.execute(any(Search.class))).thenReturn(searchResult);
        ...
    }
    */

    @Test
    public void testParseSentimentAggregation() throws IOException {
        final SearchResult searchResult = new SearchResult(new Gson());
        final String response = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("responses/sentiment_aggregation.json"));
        searchResult.setJsonString(response);
        searchResult.setJsonObject(new JsonParser().parse(response).getAsJsonObject());
        searchResult.setSucceeded(true);

        final Map<SentimentClass, Integer> distribution = AnalyticsService.parseSentimentsAggregation(searchResult);

        Assert.assertNotNull(distribution);
        Assert.assertFalse(distribution.isEmpty());
        Assert.assertEquals(7, distribution.size());

        Assert.assertFalse(distribution.containsKey(SentimentClass.Irrelevant));
        Assert.assertTrue(distribution.containsKey(SentimentClass.StrongNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.MediumNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.WeakNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.Neutral));
        Assert.assertTrue(distribution.containsKey(SentimentClass.WeakPositive));
        Assert.assertTrue(distribution.containsKey(SentimentClass.MediumPositive));
        Assert.assertTrue(distribution.containsKey(SentimentClass.StrongPositive));

        Assert.assertEquals(1, distribution.get(SentimentClass.StrongNegative).intValue());
        Assert.assertEquals(5, distribution.get(SentimentClass.MediumNegative).intValue());
        Assert.assertEquals(9, distribution.get(SentimentClass.WeakNegative).intValue());
        Assert.assertEquals(22, distribution.get(SentimentClass.Neutral).intValue());
        Assert.assertEquals(11, distribution.get(SentimentClass.WeakPositive).intValue());
        Assert.assertEquals(8, distribution.get(SentimentClass.MediumPositive).intValue());
        Assert.assertEquals(3, distribution.get(SentimentClass.StrongPositive).intValue());
    }

    @Test
    public void testParseEmptySentimentAggregation() throws IOException {
        final SearchResult searchResult = new SearchResult(new Gson());
        final String response = "{}";
        searchResult.setJsonString(response);
        searchResult.setJsonObject(new JsonParser().parse(response).getAsJsonObject());
        searchResult.setSucceeded(true);

        final Map<SentimentClass, Integer> distribution = AnalyticsService.parseSentimentsAggregation(searchResult);

        Assert.assertNotNull(distribution);
        Assert.assertFalse(distribution.isEmpty());
        Assert.assertEquals(7, distribution.size());

        Assert.assertFalse(distribution.containsKey(SentimentClass.Irrelevant));
        Assert.assertTrue(distribution.containsKey(SentimentClass.StrongNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.MediumNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.WeakNegative));
        Assert.assertTrue(distribution.containsKey(SentimentClass.Neutral));
        Assert.assertTrue(distribution.containsKey(SentimentClass.WeakPositive));
        Assert.assertTrue(distribution.containsKey(SentimentClass.MediumPositive));
        Assert.assertTrue(distribution.containsKey(SentimentClass.StrongPositive));

        Assert.assertEquals(0, distribution.get(SentimentClass.StrongNegative).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.MediumNegative).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.WeakNegative).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.Neutral).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.WeakPositive).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.MediumPositive).intValue());
        Assert.assertEquals(0, distribution.get(SentimentClass.StrongPositive).intValue());
    }

    @Test
    public void testParseSentimentSeries() throws IOException {
        final SearchResult searchResult = new SearchResult(new Gson());
        final String response = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("responses/sentiment_series.json"));
        searchResult.setJsonString(response);
        searchResult.setJsonObject(new JsonParser().parse(response).getAsJsonObject());
        searchResult.setSucceeded(true);

        final List<Pair<Integer,Double>> expectedValues = Arrays.asList(
                new ImmutablePair(43, 0.13),
                new ImmutablePair(15, 0.12),
                new ImmutablePair(29, 0.17),
                new ImmutablePair(45, 0.12),
                new ImmutablePair(24, 0.08),
                new ImmutablePair(59, 0.20),
                new ImmutablePair(69, 0.07),
                new ImmutablePair(95, 0.03),
                new ImmutablePair(126, 0.10),
                new ImmutablePair(118, 0.14),
                new ImmutablePair(119, 0.06),
                new ImmutablePair(87, 0.09),
                new ImmutablePair(59, 0.07)
        );

        final List<Pair<Integer,Double>> series = AnalyticsService.parseSentimentsSeries(searchResult);

        Assert.assertNotNull(series);
        Assert.assertFalse(series.isEmpty());
        Assert.assertEquals(expectedValues.size(), series.size());

        for (int i=0; i<series.size(); i++) {
            final Pair<Integer, Double> expected = expectedValues.get(i);
            final Pair<Integer, Double> actual = series.get(i);
            Assume.assumeNotNull(expected);
            Assert.assertEquals(expected.getKey(), actual.getKey());
            Assert.assertEquals(expected.getValue(), actual.getValue(), 0.01);
        }
    }

    @Test
    public void testParseSentimentsCount() throws IOException {
        final CountResult countResult = new CountResult(new Gson());
        final String response = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("responses/sentiment_count.json"));
        countResult.setJsonString(response);
        countResult.setJsonObject(new JsonParser().parse(response).getAsJsonObject());
        countResult.setSucceeded(true);

        final int count = AnalyticsService.parseSentimentsCount(countResult);
        Assert.assertEquals(32, count);
    }

}
