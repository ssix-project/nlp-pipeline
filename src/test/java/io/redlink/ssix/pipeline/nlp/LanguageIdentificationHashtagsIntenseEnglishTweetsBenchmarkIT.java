package io.redlink.ssix.pipeline.nlp;

import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.apache.commons.lang.StringUtils;
import org.jooq.lambda.tuple.Tuple;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Language identification for null languages benchmark test.
 *
 * See PIPELINE-299 for further details.
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(Parameterized.class)
public class LanguageIdentificationHashtagsIntenseEnglishTweetsBenchmarkIT {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentificationHashtagsIntenseEnglishTweetsBenchmarkIT.class);

    private static List<String> data;

    private final String name;
    private final LanguageIdentifier instance;

    private static final List<Tuple> report = new ArrayList<>();

    @Parameterized.Parameters
    public static Set<Map.Entry<String, LanguageIdentifier>> configs() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});
        final Map<String, LanguageIdentifier> identifiers = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, LanguageIdentifier.class);

        for (String name: new ArrayList<>(identifiers.keySet())) {
            final LanguageIdentifier identifier = identifiers.get(name);
            if (identifiers.get(name) == null) {
                log.warn("{} was null, removing it from the benchmark...", name);
                identifiers.remove(name);
            }
            if (identifier instanceof External) {
                if (!((External)identifier).isAvailable()) {
                    log.warn("{} ({}) is not available, removing it from the benchmark...", name, identifier.getClass().getCanonicalName());
                    identifiers.remove(name);
                }
            }
        }

        log.info("Benchmarking against {} language identifiers implementations:", identifiers.size());
        for (Map.Entry<String, LanguageIdentifier> entry : identifiers.entrySet()) {
            log.info("  - {} ({})", entry.getKey(), entry.getValue().getClass().getCanonicalName());
        }

        return identifiers.entrySet();
    }

    @BeforeClass
    public static void prepare() throws IOException {
        data = new ArrayList<>();
        final URL resource = LanguageIdentificationHashtagsIntenseEnglishTweetsBenchmarkIT.class.getResource("/PIPELINE-299.txt");
        try (Stream<String> stream = Files.lines(Paths.get(resource.toURI()))) {
            stream.forEach(data::add);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        log.info("loaded {} content items as test data", data.size());
    }

    @AfterClass
    public static void report() {
        final V2_AsciiTable table = new V2_AsciiTable();
        table.addRule();
        table.addRow("identifier name", "class", "count", "total time", "null", "quality");
        table.addRule();
        table.addRule();
        for (Tuple item : report) {
            table.addRow(item.toList());
            table.addRule();
        }

        final V2_AsciiTableRenderer renderer = new V2_AsciiTableRenderer();
        renderer.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
        renderer.setWidth(new WidthAbsoluteEven(180));
        System.out.println(renderer.render(table));
    }

    public LanguageIdentificationHashtagsIntenseEnglishTweetsBenchmarkIT(Map.Entry<String, LanguageIdentifier> analyzerEntry) {
        name = analyzerEntry.getKey();
        instance = analyzerEntry.getValue();
        log.info("testing against {} ({})...", name, instance.getClass().getCanonicalName());
    }

    @Before
    public void setup() throws IOException {
        Assume.assumeNotNull(instance);
        Assume.assumeTrue(data.size() > 0);
    }

    @After
    public void destroy() {

    }

    private void reporting(String name, Class clazz, int count, long time, int nulled, int right) {
        final String formattedTime = String.format("%dm %02ds %02dms", time / 60000, (time % 60000) / 1000, time % 1000);
        final String quality = String.format("%1$,.2f%%", 100.0 * right / count);
        final String nulledRate = String.format("%1$,.2f%%", 100.0 * nulled / count);
        log.info("estimated time to process {} tweets with {} was {}", data.size(), name, formattedTime);
        log.debug("average time = {}ms, with a quality rate = {}", time / count, quality);
        report.add(Tuple.tuple(name, clazz.getSimpleName(), count, formattedTime, nulledRate, quality));
    }

    @Test //PIPELINE-299
    public void testNulLLanguageIdentified() {
        int count = 0;
        int nulled = 0;
        int right = 0;

        long startTime = System.currentTimeMillis();

        for(String item: data) {
            final String language = instance.identifyLanguage(item);
            if (StringUtils.isBlank(language)) {
                nulled++;
            }
            if (StringUtils.equals(language, "en")) {
                right++;
            }
            count++;
        }

        final long estimatedTime = System.currentTimeMillis() - startTime;

        reporting(name, instance.getClass(), count, estimatedTime, nulled, right);

        Assert.assertEquals(data.size(), count);
    }

}
