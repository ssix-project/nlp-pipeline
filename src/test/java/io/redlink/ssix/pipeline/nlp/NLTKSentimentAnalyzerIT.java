package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.impl.NLTKSentimentAnalyzer;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Integration tests for the sentiment analyzer implementation relying on NLTK
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class NLTKSentimentAnalyzerIT {

    @Autowired
    private NLTKSentimentAnalyzer analyzer;

    @Before
    public void before() {
        Assume.assumeNotNull(analyzer);
        Assume.assumeNotNull(analyzer.analyze(null));
    }

    @Test
    public void testVeryPolarizedSentimentTweet() {
        final Content negativeTweet = new Content();
        negativeTweet.setContent("$AAPL is not a worthy company, all they " +
                "can sell is good image but their products are worst than the rest.");

        final Content neutralTweet = new Content();
        neutralTweet.setContent("Mean Reversion And Directional Investing " +
                "http://seekingalpha.com/article/3214256-mean-reversion-and-directional-investing?source=feed_f … $AAPL #APPLE $LL $SPY");

        final Content positiveTweet = new Content();
        positiveTweet.setContent("$UAL & $OWW have the very best software solution for flights booking!");

        final Integer negativeSentimentValue = analyzer.analyze(negativeTweet).value();
        final Integer neutralSentimentValue = analyzer.analyze(neutralTweet).value();
        final Integer positiveSentimentValue = analyzer.analyze(positiveTweet).value();

        Assert.assertTrue(negativeSentimentValue < 0);
        Assert.assertEquals(0, neutralSentimentValue, 0.2);
        Assert.assertTrue(positiveSentimentValue > 0);
    }

    @Test
    public void testRealTweetsSentimentTest() {

        final Content goodLongTermRealTweet = new Content();
        goodLongTermRealTweet.setContent(
                "QQQ Is A Good Long Term Investment " +
                        "http://seekingalpha.com/article/3213016-qqq-is-a-good-long-term-investment?source=feed_f " +
                        "$AAPL #APPLE $AMZN $IBM $NFLX $NUAN $YHOO $QQQ");
        Assert.assertEquals(400, analyzer.analyze(goodLongTermRealTweet).value(), 100);

        final Content mostValuableRealTweet = new Content();
        mostValuableRealTweet.setContent(
                "Apple retakes #1 slot as world’s most valuable brand $AAPL #aapl \n"+
                        "http://www.financialiceberg.com/apple_news_outlet.html");
        Assert.assertEquals(500, analyzer.analyze(mostValuableRealTweet).value(), 100);

        final Content atLongLastRealTweet = new Content();
        atLongLastRealTweet.setContent(
                "$AAPL - At Long Last, Apple Is Prepared to Embrace OLED Displays " +
                        "http://uk.advfn.com/news/TMF/2015/article/67034295?xref=newsalerttweet&adw=1126416 ");
        Assert.assertEquals(500, analyzer.analyze(atLongLastRealTweet).value(), 100);

        final Content howAboutRealTweet = new Content();
        howAboutRealTweet.setContent(
                "How about the guy who is worried about his $AAPL losses. You know he's going to be one hour " +
                        "Martinized soon ");
        Assert.assertEquals(-500, analyzer.analyze(howAboutRealTweet).value(), 100);

        final Content solidChoiceRealTweet = new Content();
        solidChoiceRealTweet.setContent(
                "$AAPL Is Apple Inc. (AAPL) Stock a Solid Choice Right Now? " +
                        "http://www.smarteranalyst.com/2015/05/27/is-apple-inc-aapl-stock-a-solid-choice-right-now/ ");
        Assert.assertEquals(0, analyzer.analyze(solidChoiceRealTweet).value(), 200);

        final Content gonowhereRealTweet = new Content();
        gonowhereRealTweet.setContent(
                "$AAPL continues to go nowhere after hitting multiple 161.8% Fibonacci extensions. Flat over 3 months " +
                        "if u ignored that. #OpportunityCost");
        Assert.assertEquals(-300, analyzer.analyze(gonowhereRealTweet).value(), 100);
    }

}

