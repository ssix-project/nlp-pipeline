package io.redlink.ssix.pipeline.nlp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Analyzers tools for testing the gold standard,
 * see PIPELINE-56 for further details
 *
 * @author sergio.fernandez@redlink.co
 */
public class AnalyzersGoldStandardIT {

    private static final Logger log = LoggerFactory.getLogger(AnalyzersGoldStandardIT.class);

    public static final int SAMPLE_SIZE = 1000;
    public static final int RANDOM_MAX_TRIES = 300;
    public static final String CSV_FILE = "gold_standard.csv";

    //@Autowired
    private final ObjectMapper mapper;

    private final List<Content> data;

    private final Random rnd;

    public AnalyzersGoldStandardIT() throws IOException {
        rnd = new Random();

        final SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        mapper = new ObjectMapper();
        mapper.setDateFormat(sdf);

        data = mapper.readValue(AnalyzersBenchmarkIT.class.getResourceAsStream("/tweets.json"), new TypeReference<List<Content>>(){});
        log.info("loaded {} tweets as sample data", data.size());
    }

    private Content getRandomTweet() {
        final int index = rnd.nextInt(data.size());
        return data.get(index);
    }

    public Map<String, SentimentAnalyzer> getAnalyzersInstances() {
        final ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});
        return BeanFactoryUtils.beansOfTypeIncludingAncestors(context, SentimentAnalyzer.class);
    }

    @Test
    public void testCsvGeneration() throws IOException {
        final Map<String, SentimentAnalyzer> analyzers = getAnalyzersInstances();

        final File file = new File(CSV_FILE);
        if (file.exists()) {
            file.delete();
        }
        final FileWriter writer = new FileWriter(file);
        final CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withRecordSeparator("\n"));

        final List<String> headers = new LinkedList<>();
        headers.add("uri");
        headers.add("text");
        for (Map.Entry<String, SentimentAnalyzer> analyzer: analyzers.entrySet()) {
            headers.add(analyzer.getKey());
        }
        printer.printRecord(headers);

        int count = 0;
        final Set<String> users = new HashSet<>();
        while (count < SAMPLE_SIZE) {
            final Content tweet = getRandomTweet(users);
                if (tweet != null) {
                    users.add(tweet.getAuthor());

                    final List record = new ArrayList();
                    record.add(tweet.getUri());
                    record.add(tweet.getContent());

                    for (Map.Entry<String, SentimentAnalyzer> analyzer: analyzers.entrySet()) {
                        final Sentiment sentiment = analyzer.getValue().analyze(tweet);
                        Assert.assertNotNull(sentiment);
                        record.add(sentiment.value());
                        log.debug("{} [{}={}]", tweet.getUri(), analyzer.getKey(), sentiment.value());
                    }
                    printer.printRecord(record);
                    count++;
                } else {
                    count = SAMPLE_SIZE; //exit
                }
        }

        try {
            writer.flush();
            writer.close();
            printer.close();
            log.info("CSV file dumped to {}", file.getAbsolutePath());
        } catch (IOException e) {
            log.error("Error while flushing/closing csv file: {}", e.getMessage());
        }
    }

    private Content getRandomTweet(Set<String> users) {
        Content tweet = getRandomTweet();
        int tries = 1;

        // rules (see PIPELINE-56 and PIPELINE-57):
        //  - one tweet per user
        //  - mentions more than one cashtag
        //  - contains link
        //  - no RTs
        while (users.contains(tweet.getAuthor())
                || StringUtils.countMatches(tweet.getContent(), "$") > 1
                || StringUtils.countMatches(tweet.getContent(), "https://t.co/") < 1
                || tweet.getContent().startsWith("RT ")) {
            if (++tries >= RANDOM_MAX_TRIES) {
                return null;
            }
            tweet = getRandomTweet();
        }

        return tweet;
    }

}
