package io.redlink.ssix.pipeline.nlp;

import com.opencsv.CSVReader;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.apache.commons.lang.StringUtils;
import org.jooq.lambda.tuple.Tuple;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Language identification benchmark test.
 *
 * See PIPELINE-285 for further details.
 *
 * To update the data against this benchmark runs, just download
 * https://docs.google.com/spreadsheets/d/1QOmJwh8RAnuTa_N1tBo74oRY3shTHN8P9B7NGxOIL8o
 * as CSV and replace the src/test/resources/PIPELINE-285.csv file.
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(Parameterized.class)
public class LanguageIdentificationBenchmarkIT {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentificationBenchmarkIT.class);

    private static List<String[]> data;

    private final String name;
    private final LanguageIdentifier instance;

    private static final List<Tuple> report = new ArrayList<>();

    @Parameterized.Parameters
    public static Set<Map.Entry<String, LanguageIdentifier>> configs() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});
        final Map<String, LanguageIdentifier> identifiers = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, LanguageIdentifier.class);

        for (String name: new ArrayList<>(identifiers.keySet())) {
            final LanguageIdentifier identifier = identifiers.get(name);
            if (identifiers.get(name) == null) {
                log.warn("{} was null, removing it from the benchmark...", name);
                identifiers.remove(name);
            }
            if (identifier instanceof External) {
                if (!((External)identifier).isAvailable()) {
                    log.warn("{} ({}) is not available, removing it from the benchmark...", name, identifier.getClass().getCanonicalName());
                    identifiers.remove(name);
                }
            }
        }

        log.info("Benchmarking against {} language identifiers implementations:", identifiers.size());
        for (Map.Entry<String, LanguageIdentifier> entry : identifiers.entrySet()) {
            log.info("  - {} ({})", entry.getKey(), entry.getValue().getClass().getCanonicalName());
        }

        return identifiers.entrySet();
    }

    @BeforeClass
    public static void prepare() throws IOException {
        final CSVReader reader = new CSVReader(new InputStreamReader(LanguageIdentificationBenchmarkIT.class.getResourceAsStream("/PIPELINE-285.csv")));
        data = reader.readAll();
        data.remove(0); //remove the headers
        log.info("loaded {} content items as test data", data.size());
    }

    @AfterClass
    public static void report() {
        final V2_AsciiTable table = new V2_AsciiTable();
        table.addRule();
        table.addRow("identifier name", "class", "running mode", "count", "total time", "quality");
        table.addRule();
        table.addRule();
        for (Tuple item : report) {
            table.addRow(item.toList());
            table.addRule();
        }

        final V2_AsciiTableRenderer renderer = new V2_AsciiTableRenderer();
        renderer.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
        renderer.setWidth(new WidthAbsoluteEven(180));
        System.out.println(renderer.render(table));
    }

    public LanguageIdentificationBenchmarkIT(Map.Entry<String, LanguageIdentifier> analyzerEntry) {
        name = analyzerEntry.getKey();
        instance = analyzerEntry.getValue();
        log.info("testing against {} ({})...", name, instance.getClass().getCanonicalName());
    }

    @Before
    public void setup() throws IOException {
        Assume.assumeNotNull(instance);
        Assume.assumeTrue(data.size() > 0);
    }

    @After
    public void destroy() {

    }

    private void reporting(String name, Class clazz, String runningMode, int count, long time, int right) {
        final String formattedTime = String.format("%dm %02ds %02dms", time / 60000, (time % 60000) / 1000, time % 1000);
        final String quality = String.format("%1$,.2f%%", 100.0 * right / count);
        log.info("estimated time to process {} tweets with {} was {} ({})", data.size(), name, formattedTime, runningMode);
        log.debug("average time = {}ms, with a quality rate = {}", time / count, quality);
        report.add(Tuple.tuple(name, clazz.getSimpleName(), runningMode, count, formattedTime, quality));
    }

    @Test
    public void testMonoThread() {
        int count = 0;
        int right = 0;

        long startTime = System.currentTimeMillis();

        for(String[] item: data) {
            final String language = instance.identifyLanguage(item[1]);
            if (StringUtils.equals(language, item[0])) {
                right++;
            }
            count++;
            if (count % 50 == 0) {
                log.debug("Processed {} items with {} ({})", count, name, "mono-threaded");
            }
        }

        final long estimatedTime = System.currentTimeMillis() - startTime;

        reporting(name, instance.getClass(), "mono-threaded", count, estimatedTime, right);

        Assert.assertEquals(data.size(), count);
    }

    @Test
    public void testMultiThread() {
        final int parallelism = 8;
        final AtomicInteger count = new AtomicInteger(0);
        final AtomicInteger right = new AtomicInteger(0);

        final ExecutorService executor = Executors.newFixedThreadPool(parallelism);

        long startTime = System.currentTimeMillis();

        Collection<Future> futures = new ArrayList<>();
        for (String[] item: data) {
            Callable<Boolean> task = new IdentifyLanguageTask(item[1], item[0], instance);
            Future future = executor.submit(task);
            futures.add(future);
        }

        for(Future future : futures) {
            try {
                final Boolean result = (Boolean) future.get();
                if (result) {
                    right.incrementAndGet();
                }
                count.incrementAndGet();
                if (count.get() % 50 == 0) {
                    log.debug("Processed {} items with {} ({})", count.get(), name, "multi-threaded");
                }
            } catch (InterruptedException | ExecutionException e) {
                log.error("Error: {}", e.getMessage(), e);
            }
        }

        final long estimatedTime = System.currentTimeMillis() - startTime;

        reporting(name, instance.getClass(), "multi-threaded", count.get(), estimatedTime, right.get());

        Assert.assertEquals(data.size(), count.get());

        executor.shutdown();
    }

    private class IdentifyLanguageTask implements Callable<Boolean> {

        private final String content;
        private final String expected;
        private final LanguageIdentifier identifier;

        public IdentifyLanguageTask(final String content, final String expected, final LanguageIdentifier identifier) {
            this.content = content;
            this.expected = expected;
            this.identifier = identifier;
        }

        @Override
        public Boolean call() throws Exception {
            return StringUtils.equals(expected, identifier.identifyLanguage(content));
        }

    }

}
