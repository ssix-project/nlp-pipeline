package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.impl.GateNamedEntityRecognizer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasItems;


/**
 * Integrations tests for the NER implementation relying on GATE
 *
 * @author alfonso.noriega@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class GateNamedEntityRecognizerIT {

    @Autowired
    private GateNamedEntityRecognizer ner;

    @Test
    public void testEntities() {
        assertEntities("QQQ Is A Good Long Term Investment " +
                        "http://seekingalpha.com/article/3213016-qqq-is-a-good-long-term-investment?source=feed_f " +
                        "$AAPL #APPLE $AMZN $IBM $NFLX $NUAN $YHOO $QQQ",
                "#APPLE", "$AAPL", "$AMZN", "$IBM", "$NFLX", "$NUAN", "$YHOO", "$QQQ");

        assertEntities("Apple retakes #1 slot as world’s most valuable brand $AAPL #aapl \n" +
                        "http://www.financialiceberg.com/apple_news_outlet.html",
                "$AAPL", "aapl", "Apple");

        assertEntities("$IBM going down",
                "$IBM");

        assertEntities("$GOOG is doing really great",
                "$GOOG");

    }

    private void assertEntities(String content, String... entities) {
        final Collection<String> extracted = ner.extract(content).stream()
                .map( e -> getLabels(e,content))
                .flatMap(l ->l.stream())
                .collect(Collectors.toList());
        Assert.assertThat(extracted, hasItems(entities));
    }

    private Collection<String> getLabels(Target target, String text) {

        return target.getAnnotations().stream()
                .map(a -> text.substring(new Long(a.getStart()).intValue(),new Long(a.getEnd()).intValue()))
                .collect(Collectors.toList());
    }

}

