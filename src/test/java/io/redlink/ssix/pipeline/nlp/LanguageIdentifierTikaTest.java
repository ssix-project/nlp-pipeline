package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import io.redlink.ssix.pipeline.nlp.impl.LanguageIdentifierTika;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Language Identifier Tika tests
 *
 * @author Sergio Fernández
 */
public class LanguageIdentifierTikaTest {

    final private LanguageIdentifier languageIdentifier = new LanguageIdentifierTika();

    private Content buildFakeContent(String text) {
        final Content tweet = new Content();
        final String id = RandomStringUtils.randomNumeric(18);
        final String uri = String.format("https://twitter.com/foo/status/%s", id);
        tweet.setUri(uri);
        tweet.setContent(text);
        return tweet;
    }

    private String identify(String text) {
        return languageIdentifier
                .identifyLanguage(buildFakeContent(text));
    }

    @Test
    public void testLanguageIdentificationOnRegularText() {
        Assert.assertEquals("en", identify("this is a simple text"));
        Assert.assertEquals("es", identify("esto es una prueba muy simple"));
        Assert.assertEquals("de", identify("Dies ist eine sehr einfache Test"));
    }

    @Test
    public void testLanguageIdentificationOnTweetsText() {
        Assert.assertEquals("en", identify("$AAPL I miss those old AAPL Friday Zooms of  a field goal. $210B? Just sittin there? and AAPL's transmission is still slipping in 3rd gear?"));
        Assert.assertEquals("en", identify("$AAPL is not a worthy company, all they can sell is good image but their products ar not as good as the rest."));
    }

}
