package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.nlp.impl.GeoFluentTranslator;
import org.apache.commons.collections.MapUtils;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * Integration tests for GeoFluent over our native Java client implementation
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class GeoFluentTranslatorIT {

    @Autowired
    private GeoFluentTranslator translator;

    @Before
    public void before() {
        Assume.assumeNotNull(translator);
    }

    @Test
    public void testEmptyTranslate() throws IOException, URISyntaxException {
        Assert.assertNotNull(translator.translate(null, null, null));
    }

    @Test
    public void testTranslations() {
        final String EN = "en-xn";
        final String DE = "de-de";

        final Map<String, String> translations = new HashMap<String, String>() {{
            put("this is a test", "Dies ist ein Test");
            put("I'm not sure if it would work", "Ich bin mir nicht sicher, ob es funktionieren würde");
            put("but well", "aber naja");
            put("I will test it", "Ich werde es testen");
        }};

        for (Map.Entry<String, String> entry: translations.entrySet()) {
            assertThat(translator.translate(entry.getKey(), EN, DE), equalToIgnoringCase(entry.getValue()));
        }

        final Map<String, String> invertedTranslations = MapUtils.invertMap(translations);
        for (Map.Entry<String, String> entry: invertedTranslations.entrySet()) {
            assertThat(translator.translate(entry.getKey(), "de", "en"), equalToIgnoringCase(entry.getValue()));
        }
    }

    @Test
    public void testUnsupportedLanguages() {
        assertEquals(translator.translate("test", "foo", "en"), "");
        assertThat(translator.translate("Dies ist ein Test", "de", "en"), equalToIgnoringCase("this is a test"));
    }

}

