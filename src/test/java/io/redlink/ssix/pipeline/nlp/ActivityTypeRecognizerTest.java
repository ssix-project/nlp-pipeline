package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Prediction;
import io.redlink.ssix.pipeline.nlp.api.EventRecognizer;
import io.redlink.ssix.pipeline.nlp.impl.ActivityTypeRecognizer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by wikier on 21/03/17.
 */
public class ActivityTypeRecognizerTest {

    private EventRecognizer eventRecognizer;

    @Before
    public void before() {
        eventRecognizer = new ActivityTypeRecognizer();
    }

    @After
    public void after() {
        eventRecognizer = null;
    }

    @Test
    public void testEdgeCases() {
        Assert.assertNull(eventRecognizer.predict(null));
        Assert.assertNull(eventRecognizer.predict(""));
        Assert.assertNull(eventRecognizer.predict(" "));
    }

    @Test
    public void testActivities() {
        Assert.assertEquals(Prediction.Deal, eventRecognizer.predict("$FB acquires #VR audio company Two Big Ears"));
        Assert.assertEquals(Prediction.Announcement, eventRecognizer.predict("$FB announced an open-source 360-degree video camera <url>..</url> #F82016"));
        Assert.assertEquals(Prediction.Other, eventRecognizer.predict("$FB Can't wait to see the numbers. $AAPL Looks cheap before the earnings. #stocks #investing #tradeideas"));
        Assert.assertEquals(Prediction.Other, eventRecognizer.predict("$FB -- Facebook CEO Mark Zuckerberg Says Will Allow Developers to Build Chatbots on Messenger"));
        Assert.assertEquals(Prediction.Other, eventRecognizer.predict("$FB Facebook CEO Mark Zuckerberg Slams Donald Trump <url>..</url> $FB gains is all about politics and so it the attack on $TWTR"));
        Assert.assertEquals(Prediction.Deal, eventRecognizer.predict(".@EMCcorp says Dell deal still on track despite missing expectations <url>..</url> $EMC <url>..</url>"));
        Assert.assertEquals(Prediction.Appointment, eventRecognizer.predict("$ENDP #2 top holder TPG Group changed filing status to be ACTIVE today; appointed Todd Sisitsky, a TPG Principal, to be a board member."));
        Assert.assertEquals(Prediction.Release, eventRecognizer.predict("Eli Lilly wants to launch 20 drugs by 2023 in R&amp;D push - new focus areas incl. immunology, non-opioid pain meds <url>..</url> $LLY"));
        Assert.assertEquals(Prediction.Other, eventRecognizer.predict("this is a test"));
    }

}
