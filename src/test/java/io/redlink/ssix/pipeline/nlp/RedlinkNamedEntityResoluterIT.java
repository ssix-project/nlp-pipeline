package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Annotation;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.model.TextPos;
import io.redlink.ssix.pipeline.nlp.impl.RedlinkNamedEntityResoluter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasItems;


/**
 * Integrations tests for the NER implementation relying on Redlink
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class RedlinkNamedEntityResoluterIT {

    @Autowired
    private RedlinkNamedEntityResoluter nel;

    @Test
    public void testEntities() {
        /*assertEntitiesResolution("QQQ Is A Good Long Term Investment " +
                        "http://seekingalpha.com/article/3213016-qqq-is-a-good-long-term-investment?source=feed_f " +
                        "$AAPL #APPLE $AMZN $IBM $NFLX $NUAN $YHOO $QQQ",
                "aapl", "ibm", "amzn", "qqq");

        assertEntitiesResolution("Apple retakes #1 slot as world’s most valuable brand $AAPL #aapl \n" +
                        "http://www.financialiceberg.com/apple_news_outlet.html",
                "aapl");

        assertEntitiesResolution("$IBM going down",
                "ibm");

        assertEntitiesResolution("$GOOG is doing really great",
                "goog");*/

        final Target target = new Target();
        target.setEntity_name("$AAPL");
        target.setEntity_id("001");
        target.setParent_id("000");
        target.addAnnotation(new Annotation().setId(0).setType("Stock").setStart(0).setEnd(4));

        final Target target2 = new Target();
        target2.setEntity_name("Apple Inc.");
        target2.setEntity_id("000");
        target2.addAnnotation(new Annotation().setId(1).setType("Company").setStart(6).setEnd(10));

        assertEntitiesResolution(Arrays.asList(target, target2),
                "Apple Inc.");

        assertEntitiesLinking(Arrays.asList(target, target2),
                "http://dbpedia.org/resource/Apple_Inc.");

    }

    private void assertEntitiesResolution(Collection <Target> targets, String... entities) {
        final Collection<String> extracted = nel.analyze(targets).stream()
                .map( e -> e.getEntity_name())
                .collect(Collectors.toList());
        Assert.assertThat(extracted, hasItems(entities));
    }

    private void assertEntitiesLinking(Collection <Target> targets, String... uris) {
        final Collection<String> extracted = nel.analyze(targets).stream()
                .map( e -> e.getUri())
                .collect(Collectors.toList());
        Assert.assertThat(extracted, hasItems(uris));
    }

}

