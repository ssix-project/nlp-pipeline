package io.redlink.ssix.pipeline.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class ContentTest {

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testDeserialization() throws IOException {
        final String JSON = "{\n" +
                "  \"uri\":\"https://twitter.com/Adam1Baum/status/739816598588923905\",\n" +
                "  \"author\":\"Adam1Baum\",\n" +
                "  \"source\":\"twitter\",\n" +
                "  \"content\":\"@WendyM53 The HMS #EU's last voyage #eureferendum https://t.co/2uCnFN9BNe\",\n" +
                "  \"date\":1465220996000,\n" +
                "  \"geo\":null,\n" +
                "  \"shared\":false,\n" +
                "  \"sentiment\":324,\n" +
                "  \"language\":\"enr\"\n" +
                "}";

        final Map<String, Object> json = mapper.readValue(JSON, Map.class);
        final Content content = mapper.readValue(JSON, Content.class);
        Assert.assertEquals(json.get("uri"), content.getUri());
        Assert.assertEquals(json.get("author"), content.getAuthor());
        Assert.assertEquals(json.get("content"), content.getContent());
        Assert.assertEquals(json.get("date"), content.getDate());
        Assert.assertEquals(json.get("geo"), content.getGeo());
        Assert.assertEquals(json.get("shared"), content.isShared());
        Assert.assertEquals(json.get("sentiment"), content.getSentiment());
        Assert.assertEquals(json.get("language"), content.getLanguage());
    }

    @Test // PLATFORM-158
    public void testSharedPolymorphism() throws IOException {
        final String JSON_1 = "{" +
                "  \"uri\":\"https://twitter.com/computer_hware/status/831075828452593665\",\n" +
                "  \"author\":\"computer_hware\",\n" +
                "  \"verified\":false,\n" +
                "  \"favourites\":0,\n" +
                "  \"followers\":170,\n" +
                "  \"following\":2,\n" +
                "  \"source\":\"twitter\",\n" +
                "  \"content\":\"#Apple Bay Area companies poised to reap benefits of autonomous cars. Read more: https://t.co/8FJJfEBTwh $AAPL\",\"date\":\"1486978892000\",\n" +
                "  \"geo\":\"San Mateo, CA\",\n" +
                "  \"shared\":true" +
                "}";

        final Content content1 = mapper.readValue(JSON_1, Content.class);
        Assert.assertTrue(content1.isShared());

        final String JSON_2 = "{" +
                "  \"uri\":\"https://twitter.com/computer_hware/status/831075828452593665\",\n" +
                "  \"author\":\"computer_hware\",\n" +
                "  \"verified\":false,\n" +
                "  \"favourites\":0,\n" +
                "  \"followers\":170,\n" +
                "  \"following\":2,\n" +
                "  \"source\":\"twitter\",\n" +
                "  \"content\":\"#Apple Bay Area companies poised to reap benefits of autonomous cars. Read more: https://t.co/8FJJfEBTwh $AAPL\",\"date\":\"1486978892000\",\n" +
                "  \"geo\":\"San Mateo, CA\",\n" +
                "  \"retweet\":true" +
                "}";

        final Content content2 = mapper.readValue(JSON_2, Content.class);
        Assert.assertTrue(content2.isShared());
    }

    @Test
    public void testIdExtractionFromUri() throws IOException {
        final String JSON_1 = "{\n" +
                "  \"uri\":\"https://twitter.com/Adam1Baum/status/739816598588923905\",\n" +
                "  \"author\":\"Adam1Baum\",\n" +
                "  \"source\":\"twitter\",\n" +
                "  \"content\":\"@WendyM53 The HMS #EU's last voyage #eureferendum https://t.co/2uCnFN9BNe\",\n" +
                "  \"date\":1465220996000,\n" +
                "  \"geo\":null,\n" +
                "  \"shared\":false,\n" +
                "  \"sentiment\":324,\n" +
                "  \"language\":\"enr\"\n" +
                "}";

        final Content content1 = mapper.readValue(JSON_1, Content.class);
        Assert.assertEquals("https://twitter.com/Adam1Baum/status/739816598588923905", content1.getUri());
        Assert.assertEquals("739816598588923905", content1.getId());

        final String JSON_2 = "{\n" +
                "  \"id\":\"1\",\n" +
                "  \"uri\":\"https://twitter.com/Adam1Baum/status/739816598588923905\",\n" +
                "  \"author\":\"Adam1Baum\",\n" +
                "  \"source\":\"twitter\",\n" +
                "  \"content\":\"@WendyM53 The HMS #EU's last voyage #eureferendum https://t.co/2uCnFN9BNe\",\n" +
                "  \"date\":1465220996000,\n" +
                "  \"geo\":null,\n" +
                "  \"shared\":false,\n" +
                "  \"sentiment\":324,\n" +
                "  \"language\":\"enr\"\n" +
                "}";

        final Content content2 = mapper.readValue(JSON_2, Content.class);
        Assert.assertEquals("https://twitter.com/Adam1Baum/status/739816598588923905", content2.getUri());
        Assert.assertEquals("1", content2.getId());
    }

    @Test //PLATFORM-181
    public void testAddTargets() {
        final Content content = new Content();
        content.addTarget("rio", 0);
        content.addTarget("at", 183);
        content.addTarget("hseb", 183);
        content.addTarget("rio", 183);
        Assert.assertEquals(3, content.getTargets().size());
    }

}
