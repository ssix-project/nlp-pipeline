package io.redlink.ssix.pipeline.model;

import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import org.junit.Assert;
import org.junit.Test;

public class SentimentTest {

    @Test
    public void testParsing() {
        Assert.assertEquals(0, new Sentiment().value());
        Assert.assertEquals(0, new Sentiment(0).value());
        Assert.assertEquals(0, new Sentiment(0.0).value());
        Assert.assertEquals(-300, new Sentiment(-300).value());
        Assert.assertEquals(-300, new Sentiment(-0.3).value());
        Assert.assertEquals(300, new Sentiment(300).value());
        Assert.assertEquals(300, new Sentiment(0.3).value());
        Assert.assertEquals(-1000, new Sentiment(-1000).value());
        Assert.assertEquals(-1000, new Sentiment(-1.0).value());
        Assert.assertEquals(-1, new Sentiment(-1).value());
        Assert.assertEquals(1000, new Sentiment(1000).value());
        Assert.assertEquals(1000, new Sentiment(1.0).value());
        Assert.assertEquals(1, new Sentiment(1).value());
        Assert.assertEquals(-1000, new Sentiment(-1189).value());
        Assert.assertEquals(-1000, new Sentiment(-1.189).value());
        Assert.assertEquals(1000, new Sentiment(1189).value());
        Assert.assertEquals(1000, new Sentiment(1.189).value());
    }

}
