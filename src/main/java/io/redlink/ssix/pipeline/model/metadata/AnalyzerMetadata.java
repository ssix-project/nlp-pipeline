/*
 * Copyright (c) 2017 Redlink GmbH.
 */
package io.redlink.ssix.pipeline.model.metadata;

import java.util.Date;

/**
 * Created on 21.12.17.
 */
public class AnalyzerMetadata {

    private long start = new Date().getTime();
    private long end;

    private String name;
    private boolean enabled = true;

    public long getStart() {
        return start;
    }

    public AnalyzerMetadata setStart(long start) {
        this.start = start;
        return this;
    }

    public long getEnd() {
        return end;
    }

    public AnalyzerMetadata setEnd(long end) {
        this.end = end;
        return this;
    }

    public String getName() {
        return name;
    }

    public AnalyzerMetadata setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public AnalyzerMetadata setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }
}
