package io.redlink.ssix.pipeline.model;

/**
 * Prediction Types
 *
 * @author Sergio Fernández
 * @author Gopal KS
 */
public enum Prediction {

    Announcement,

    Appointment,

    Deal,

    Downsize,

    Hiring,

    Market,

    Release,

    Other

}
