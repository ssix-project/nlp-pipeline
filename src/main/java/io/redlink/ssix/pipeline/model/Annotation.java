package io.redlink.ssix.pipeline.model;

/**
 * Created by fonso on 08.06.17.
 */
public class Annotation {

    private int id;
    private String type;
    private long start;
    private long end;

    public Annotation() {
    }

    public Annotation setId(int id) {
        this.id = id;
        return this;
    }

    public Annotation setType(String type) {
        this.type = type;
        return this;
    }

    public Annotation setStart(long start) {
        this.start = start;
        return this;
    }

    public Annotation setEnd(long end) {
        this.end = end;
        return this;
    }

    public String getType() {
        return type;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public int getId() {
        return id;
    }
}
