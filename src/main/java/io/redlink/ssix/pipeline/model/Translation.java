package io.redlink.ssix.pipeline.model;

/**
 * Translation bean
 *
 * @author sergio.fernandez@redlink.co
 */
public class Translation {

    private String text;

    private String source;

    private String target;

    public String getText() {
        return text;
    }

    public void setTranslation(String text) {
        this.text = text;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
