package io.redlink.ssix.pipeline.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple result for retrieving analysis from different microservices
 *
 * @author sergio.fernandez@redlink.co
 */
public class RestResult {

    private double sentiment;

    private Translation translation;

    private String data;

    private String timestamp;

    private Map<String, Target> targets;
    private Collection<Annotation> annotations;

    private Map<String, Aspect> aspects;

    public RestResult() {
        targets = new HashMap<>();
        aspects = new HashMap<>();
    }

    public double getSentiment() {
        return sentiment;
    }

    public void setSentiment(double sentiment) {
        this.sentiment = sentiment;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Target> getTargets() {
        return targets;
    }

    public void setTargets(Map<String, Target> targets) {
        this.targets = new HashMap<>(targets);
    }

    public Collection<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Collection<Annotation> annotations) {
        this.annotations = annotations;
    }

    public Map<String, Aspect> getAspects() {
        return aspects;
    }

    public void setAspects(Map<String, Aspect> aspects) {
        this.aspects = new HashMap<>(aspects);
    }

}
