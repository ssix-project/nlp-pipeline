/*
 * Copyright (c) 2017 Redlink GmbH.
 */
package io.redlink.ssix.pipeline.model.metadata;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 21.12.17.
 */
public class Metadata {

    private final long start = new Date().getTime();
    private long end;

    private final Map<String, AnalyzerMetadata> analyzers = new HashMap<>();

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public Metadata setEnd(long end) {
        this.end = end;
        return this;
    }

    public Map<String, AnalyzerMetadata> getAnalyzers() {
        return analyzers;
    }

    public Metadata addAnalizer(String analyzerClassName, AnalyzerMetadata ma ) {
        analyzers.put(analyzerClassName,ma);
        return this;
    }
}
