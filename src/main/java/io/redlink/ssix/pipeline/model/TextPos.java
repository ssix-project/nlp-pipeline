package io.redlink.ssix.pipeline.model;

/**
 * Created by fonso on 08.06.17.
 */
public class TextPos {
    private int id;
    private int offset;

    public TextPos() {
    }

    public int getId() {
        return id;
    }

    public TextPos setId(int id) {
        this.id = id;
        return this;
    }

    public int getOffset() {
        return offset;
    }

    public TextPos setOffset(int offset) {
        this.offset = offset;
        return this;
    }
}
