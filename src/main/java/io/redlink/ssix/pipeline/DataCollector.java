package io.redlink.ssix.pipeline;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.User;
import io.redlink.ssix.pipeline.clients.ElasticSearchClientBuilder;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.client.JestResultHandler;
import io.searchbox.core.Index;
import io.searchbox.indices.CreateIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;

public class DataCollector implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(DataCollector.class);

    public static final String INDEX = "twitter";

    private String esHost;
    private int esPort;
    private String esIndex;
    transient JestClient esClient;

    public void init() {
        try {
            final JestClient client = getElasticSearchClient();
            client.execute(new CreateIndex.Builder(INDEX).build());
            esClient = client;
        } catch (IOException e) {
            log.error("Error creating base index on ES: {}", e.getMessage(), e);
            esClient = null;
        }
    }

    public void destroy() {
        if (esClient != null) {
            esClient.shutdownClient();
            esClient = null;
            log.info("Destroyed ES client");
        }
    }

    private synchronized JestClient getElasticSearchClient() {
        if (esClient == null) {
            esClient = ElasticSearchClientBuilder.build(esHost, esPort);
        }
        return esClient;
    }

    public synchronized void put(Content content) {
        cacheResult(content);
        //TODO: more?
    }

    /**
     * Cache a result
     *
     * @param content
     */
    private void cacheResult(final Content content) {
        final JestClient client = getElasticSearchClient();
        if (client != null) {
            final Index contentIndex = new Index.Builder(content).index(esIndex).type("content").build();
            client.executeAsync(contentIndex, new JestResultHandler<JestResult>() {
                @Override
                public void completed(JestResult result) {
                    log.debug("Completed indexation of content {} with succeeded={}", content.getUri(), result.isSucceeded());
                }

                @Override
                public void failed(Exception e) {
                    log.error("Error indexing content {}: {}", content.getUri(), e.getMessage(), e);
                }
            });

            final User user = new User(content);
            final Index userIndex = new Index.Builder(user).index(esIndex).type("content").build();
            client.executeAsync(userIndex, new JestResultHandler<JestResult>() {
                @Override
                public void completed(JestResult result) {
                    log.debug("Completed indexation of user {} with succeeded={}", user.getUsername(), result.isSucceeded());
                }

                @Override
                public void failed(Exception e) {
                    log.error("Error indexing user {}: {}", user.getUsername(), e.getMessage(), e);
                }
            });
        } else {
            log.warn("Content {} won't be cached, there is not target bucket", content.getUri());
        }
    }

    public void setEsPort(int esPort) {
        this.esPort = esPort;
    }

    public void setEsHost(String esHost) {
        this.esHost = esHost;
    }

    public void setEsIndex(String esIndex) {
        this.esIndex = esIndex;
    }
}
