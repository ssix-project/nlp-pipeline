package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.nlp.api.NamedEntityResoluter;
import io.redlink.ssix.pipeline.util.PipelineFunction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class NELFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(NELFunction.class);

    private NamedEntityResoluter namedEntityLinker;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(NEL.name()) ||
                (analyzers.containsKey(NEL.name()) && analyzers.get(NEL.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new AnalyzerMetadata()
                    .setName(namedEntityLinker.getClass().getCanonicalName());

            analyzers.put(NEL.name(), metadata);
        }


        final AnalyzerMetadata metadata = analyzers.get(NEL.name());
        if (metadata.isEnabled()) {
            metadata.setStart(new Date().getTime());
            if(CollectionUtils.isNotEmpty(content.getTargets())) {
                Collection<Target> entities = namedEntityLinker.analyze(content.getTargets());

                if (Objects.isNull(entities)) {
                    log.error("Error during analysis with {}: {}", this.getClass().getCanonicalName(), "Unexpected null response from NEL component.");
                    entities = Collections.EMPTY_LIST;
                }

                log.debug("{} entities after resolution from content {}", entities.size(), content.getUri());
                content.setTargets(new ArrayList(entities));
            }
            metadata.setEnd(new Date().getTime());
        }

        return content;
    }

    public void setNamedEntityLinker(NamedEntityResoluter namedEntityLinker) {
        this.namedEntityLinker = namedEntityLinker;
    }

}
