package io.redlink.ssix.pipeline.functions;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.Content;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import java.io.IOException;

public class JsonParseFunction implements Function<Tuple2<String,String>,Content> {

    private static final Logger log = LoggerFactory.getLogger(JsonParseFunction.class);

    private ObjectMapper mapper;

    @Override
    public Content call(Tuple2<String, String> rawContent) {
        log.info("Input raw content from batch {}", rawContent._2());
        try {
            return this.mapper.readValue(rawContent._2(), Content.class);
        } catch (IOException e) {
            log.error("Exception thrown while parsing raw content with jackson mapper: {}", e.getMessage(), e);
            return new Content(); //TODO
        }
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

}
