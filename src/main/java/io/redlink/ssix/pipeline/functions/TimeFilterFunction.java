package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Time filter functionime filter function
 *
 * @author alfonso.noriega@redlink.co
 * @author sergio.fernandez@redlink.co
 */
public class TimeFilterFunction implements Function<Content,Boolean> {

    private static final Logger log = LoggerFactory.getLogger(TimeFilterFunction.class);

    private long startTime;

    public void setStartTime(Date startTime) {
        this.startTime = startTime.getTime();
    }

    @Override
    public Boolean call(Content tweet) throws Exception {
        if ((Long)tweet.getDate() != null) {
            final boolean check = tweet.getDate() >= startTime;
            log.debug("time check for content {} is {} >= {} => {}", tweet.getUri(), tweet.getDate(), startTime, check);
            return check;
        } else {
            log.error("content {} does not come with timestamp!", tweet.getUri());
            return false;
        }
    }

}
