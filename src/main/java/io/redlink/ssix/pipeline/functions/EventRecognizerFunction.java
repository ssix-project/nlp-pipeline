package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.Prediction;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.nlp.api.EventRecognizer;
import io.redlink.ssix.pipeline.util.PipelineFunction;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class EventRecognizerFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(EventRecognizerFunction.class);

    private EventRecognizer eventRecognizer;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(EVENT.name()) ||
                (analyzers.containsKey(EVENT.name()) && analyzers.get(EVENT.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new AnalyzerMetadata()
                    .setName(eventRecognizer.getClass().getCanonicalName());

            analyzers.put(EVENT.name(), metadata);
        }

        final AnalyzerMetadata metadata = analyzers.get(EVENT.name());
        if (metadata.isEnabled()) {
            metadata.setStart(new Date().getTime());
            final Prediction prediction = eventRecognizer.predict(content.getContent().replaceAll("https://t.co/[a-zA-Z0-9]+", ""));
            if (prediction != null) {
                content.setEvent(prediction.name());
                log.debug("Predicted '{}' event from content {}", prediction, content.getUri());
            } else {
                log.debug("No event predicted from content {}", content.getUri());
            }
            metadata.setEnd(new Date().getTime());
        }
        return content;
    }

    public void setEventRecognizer(EventRecognizer eventRecognizer) {
        this.eventRecognizer = eventRecognizer;
    }

}
