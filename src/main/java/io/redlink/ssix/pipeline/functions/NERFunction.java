package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.nlp.api.NamedEntityRecognizer;
import io.redlink.ssix.pipeline.util.PipelineFunction;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class NERFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(NERFunction.class);

    private NamedEntityRecognizer namedEntityRecognizer;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(NER.name()) ||
                (analyzers.containsKey(NER.name()) && analyzers.get(NER.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new AnalyzerMetadata()
                    .setName(namedEntityRecognizer.getClass().getCanonicalName());

            analyzers.put(NER.name(), metadata);
        }

        final AnalyzerMetadata metadata = analyzers.get(NER.name());
        if (metadata.isEnabled()) {
            metadata.setStart(new Date().getTime());
            Collection<Target> entities = Collections.EMPTY_LIST;
            if (Objects.nonNull(content)){
                entities = namedEntityRecognizer.extract(content.getContent().replaceAll("https://t.co/[a-zA-Z0-9]+", ""));
                //PIPELINE-351: Safe check to avoid pipeline breakdown in case of NER component failure.
                if (Objects.isNull(entities)) {
                    log.error("Error during analysis with {}: {}", this.getClass().getCanonicalName(), "Unexpected null response from NER component.");
                    entities = Collections.EMPTY_LIST;
                }
            }
            log.debug("Extracted {} entities from content {}", entities.size(), content.getUri());
            content.setTargets(new ArrayList(entities));

            metadata.setEnd(new Date().getTime());
        }

        return content;
    }

    public void setNamedEntityRecognizer(NamedEntityRecognizer namedEntityRecognizer) {
        this.namedEntityRecognizer = namedEntityRecognizer;
    }

}
