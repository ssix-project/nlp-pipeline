package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import io.redlink.ssix.pipeline.util.PipelineFunction;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class SentimentFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(SentimentFunction.class);

    private SentimentAnalyzer domainSentimentAnalyzer;
    private SentimentAnalyzer genericSentimentAnalyzer;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(SENTIMENT.name()) ||
                (analyzers.containsKey(SENTIMENT.name()) && analyzers.get(SENTIMENT.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new AnalyzerMetadata();

            analyzers.put(SENTIMENT.name(), metadata);
        }

        final AnalyzerMetadata metadata = analyzers.get(SENTIMENT.name());
        if (metadata.isEnabled()) {
            metadata.setStart(new Date().getTime());
            final boolean genericContent = content.getTargets().stream()
                    .filter( t -> Objects.nonNull(t.getDomain()))
                    .noneMatch(t -> t.getDomain().toLowerCase().equals("Finance".toLowerCase()));

            final Sentiment sentiment;
            if (Objects.nonNull(content)) {
                //Remove URLs from content
                final String originalContent = content.getContent();
                content.setContent(originalContent.replaceAll("https://t.co/[a-zA-Z0-9]+", ""));

                if(genericContent) {
                    metadata.setName(genericSentimentAnalyzer.getClass().getCanonicalName());
                    log.info("Content analyzed by generic sentiment analyzer: {}", genericSentimentAnalyzer.getClass().getCanonicalName() );

                    sentiment = genericSentimentAnalyzer.analyze(content);
                } else {
                    metadata.setName(domainSentimentAnalyzer.getClass().getCanonicalName());
                    log.info("Content analyzed by domain specific sentiment analyzer: {}", domainSentimentAnalyzer.getClass().getCanonicalName() );
                    sentiment = domainSentimentAnalyzer.analyze(content);
                }
                if (Objects.nonNull(sentiment)) {
                    //Restore content with URLs
                    content.setContent(originalContent);
                    content.setSentiment(sentiment.value());
                    log.debug("sentiment for content {} is {}", content.getUri(), content.getSentiment());

                    for (Map.Entry<String, Integer> entry : sentiment.getTargets().entrySet()) {
                        final Target target = content.getTarget(entry.getKey());
                        if (Objects.nonNull(target)) {
                            target.setSentiment(entry.getValue());
                        } else {
                            content.addTarget(entry.getKey(), entry.getValue());
                        }
                    }
                } else {
                    log.error("No sentiment extracted for content {}", content.getUri());
                    //FIXME: find a proper way to redo or ignore this content
                }
            }


            metadata.setEnd(new Date().getTime());
        }
        return content;
    }

    public SentimentAnalyzer getDomainSentimentAnalyzer() {
        return domainSentimentAnalyzer;
    }

    public void setDomainSentimentAnalyzer(SentimentAnalyzer domainSentimentAnalyzer) {
        this.domainSentimentAnalyzer = domainSentimentAnalyzer;
    }

    public SentimentAnalyzer getGenericSentimentAnalyzer() {
        return genericSentimentAnalyzer;
    }

    public void setGenericSentimentAnalyzer(SentimentAnalyzer genericSentimentAnalyzer) {
        this.genericSentimentAnalyzer = genericSentimentAnalyzer;
    }
}
