package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.nlp.impl.RandomSentimentAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.Map;

/**
 * Sentiment Analyzer Factory
 *
 * @author Sergio Fernández
 */
public class SentimentAnalyzerFactory {

    private final Class<? extends SentimentAnalyzer> DEFAULT = RandomSentimentAnalyzer.class;

    @Autowired
    private ApplicationContext context;

    /**
     * Get an instance of the default SentimentAnalyzer.
     *
     * @return
     */
    public SentimentAnalyzer getSentimentAnalyzer() {
        return getSentimentAnalyzer(DEFAULT);
    }

    /**
     * Get an instance of the SentimentAnalyzer class requested.
     * Quite inspired by BeanExpressionContext.getObject() behaviour.
     *
     * @param klass
     * @return
     */
    public SentimentAnalyzer getSentimentAnalyzer(Class<? extends SentimentAnalyzer> klass) {
        final Map<String, ? extends SentimentAnalyzer> beans = context.getBeansOfType(klass);
        if (beans.isEmpty()) {
            return getSentimentAnalyzer(); //FIXME
        } else {
            return beans.values().iterator().next();
        }
    }

    /**
     * Get the instance of the SentimentAnalyzer with that name.
     * Quite inspired by BeanExpressionContext.getObject() behaviour.
     *
     * @param name
     * @return
     */
    public SentimentAnalyzer getSentimentAnalyzer(String name) {
        final Object bean = context.getBean(name);
        if (bean instanceof SentimentAnalyzer) {
            return (SentimentAnalyzer) bean;
        } else {
            return getSentimentAnalyzer(); //FIXME
        }
    }

}
