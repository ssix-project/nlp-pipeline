package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Target;

import java.io.Serializable;
import java.util.Collection;

/**
 * Named-EntityResolutor interface
 *
 * @author Alfonso Noriega Meneses
 */
public interface NamedEntityResoluter extends Serializable {

    void init();

    Collection<Target> analyze(Collection<Target> entities);

    void destroy();

}
