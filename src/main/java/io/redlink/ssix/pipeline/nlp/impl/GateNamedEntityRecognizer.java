package io.redlink.ssix.pipeline.nlp.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.RestResult;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.NamedEntityRecognizer;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * Created by fonso on 08.06.17.
 */
public class GateNamedEntityRecognizer implements NamedEntityRecognizer,External {

    private static final Logger log = LoggerFactory.getLogger(GateNamedEntityRecognizer.class);

    private String host;
    private int port;
    private ObjectMapper mapper;

    protected boolean available;

    protected CloseableHttpClient createClient() {
        return HttpClients.createDefault();
    }

    @Override
    public void init() {
        try(CloseableHttpClient client = createClient()) { //TODO: reuse client
            URIBuilder builder = new URIBuilder();
            builder.setScheme("http").setHost(this.host).setPort(this.port);
            URI uri = builder.build();
            final HttpOptions options = new HttpOptions(uri);
            final CloseableHttpResponse response = client.execute(options);
            if (response.getStatusLine().getStatusCode() == 200) {
                log.info("Initialized GateNamedEntityRecognizer client over '{}'", uri);
                setAvailable(true);
            } else {
                setAvailable(false);
                throw new IOException("GateNamedEntityRecognizer server not found at " + uri);
            }
        } catch (IOException e) {
            log.error("Error initializing GateNamedEntityRecognizer client: {}", e.getMessage());
            //throw new IllegalStateException(e);
        } catch (URISyntaxException e) {
            log.error("Error initializing GateNamedEntityRecognizer client: {}", e.getMessage());
        }
    }

    @Override
    public void destroy() {
        available = false;
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available =available;
    }

    @Override
    public Collection<Target> extract(String content) {
        if (StringUtils.isBlank(content)) {
            log.warn("empty/null content, so we skip the call");
            return Collections.EMPTY_LIST;
        } else {
            try {

                Collection<Target> entities = this.getEntities(content);

                // Retrying connection to NER microservice if result was null.
                int attempts = 1;
                while (Objects.isNull(entities) && attempts < 4) {
                    log.warn("Named entity recognition attempt {} failed: repeat connection to NER micro-service.", attempts);
                    entities = this.getEntities(content);
                    attempts++;
                }
                return Objects.nonNull(entities)? entities : Collections.EMPTY_LIST;
            } catch (URISyntaxException e) {
                log.error("Error during analysis with {}: {}", this.getClass().getCanonicalName(), e.getMessage(), e);
                log.debug("Content was: \n{}", content);
                return Collections.EMPTY_LIST;
            }
        }
    }

    private Collection<Target> getEntities(String content) throws URISyntaxException {

        try (CloseableHttpClient client = createClient()) {
            final URIBuilder builder = new URIBuilder();
            builder.setScheme("http").setHost(this.host).setPort(this.port)
                    .setParameter("text", content);
            final URI uri = builder.build();
            final HttpGet get = new HttpGet(uri);
            log.debug("GATE Named Entity Recognition for text '{}' against '{}://{}'", content, uri.getScheme(), uri.getAuthority());
            final Collection<Target> targets = client.execute(get, getResponseHandler());
            return targets;
        } catch (IOException e) {
            log.error("Error during analysis with {}: {}", this.getClass().getCanonicalName(), e.getMessage(), e);
            log.debug("Content was: \n{}", content);
            return null;
        }
    }

    public ResponseHandler<Collection<Target>> getResponseHandler() {
        return response -> {
            final RestResult result = mapper.readValue(response.getEntity().getContent(), RestResult.class);
            return result.getTargets().values();
        };
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }
}
