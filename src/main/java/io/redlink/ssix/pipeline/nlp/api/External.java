package io.redlink.ssix.pipeline.nlp.api;

/**
 * Analyzer that relies on a external service
 *
 * @author Sergio Fernández
 */
public interface External {

    boolean isAvailable();

    void setAvailable(boolean available);

}
