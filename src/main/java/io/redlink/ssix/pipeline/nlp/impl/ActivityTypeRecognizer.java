package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.pipeline.model.Prediction;
import io.redlink.ssix.pipeline.nlp.api.EventRecognizer;
import opennlp.maxent.io.GISModelReader;
import opennlp.model.BinaryFileDataReader;
import opennlp.model.MaxentModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * An implementation of activity type detection based on OpenNLP maxent.
 *
 * @author Gopal
 * @author alanna.kelly@inisight-centre.org
 */
public class ActivityTypeRecognizer implements EventRecognizer {

    private static final Logger log = LoggerFactory.getLogger(ActivityTypeRecognizer.class);

    /**
     * Maxent predictor model
     */
    private MaxentModel model = null;

    @Override
    public Prediction predict(String content) {

        /*
         * Lazy load of GIS model
         */
        if (model == null) {
            try {
                InputStream is = new GZIPInputStream(this.getClass().getResourceAsStream("/event-recognizer-model/ActivityType_Model_batch5.bin.gz"));
                GISModelReader loader = new GISModelReader(new BinaryFileDataReader(is));
                model = loader.getModel();
                if (model == null) {
                    throw new RuntimeException("Attempted to load model, but model is null after loading");
                }
            } catch (IOException e) {
                log.error("Unable to load event recognition model: {}", e.getMessage(), e);
            }
        }

        if (StringUtils.isNotBlank(content)) {
            final String bestOutcome = model.getBestOutcome(model.eval(content.split(" ")));
            try {
                return Prediction.valueOf(bestOutcome);
            } catch (IllegalArgumentException e) {
                log.error("Unable to parse '{}' prediction: {}", bestOutcome, e.getMessage(), e);
                return null; //FIXME
            }
        } else {
            log.warn("Skipped analysing empty content");
            return null; //FIXME
        }
    }

}
