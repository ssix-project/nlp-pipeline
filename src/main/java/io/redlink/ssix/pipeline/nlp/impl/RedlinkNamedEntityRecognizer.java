package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.sdk.RedLink;
import io.redlink.sdk.RedLinkFactory;
import io.redlink.sdk.impl.analysis.AnalysisRequest;
import io.redlink.sdk.impl.analysis.model.Enhancements;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.NamedEntityRecognizer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Named-Entity Recognizer relying on the Redlink Platform
 *
 * @author Sergio Fernández
 */
public class RedlinkNamedEntityRecognizer implements NamedEntityRecognizer, External {

    private static final Logger log = LoggerFactory.getLogger(RedlinkNamedEntityRecognizer.class);

    private String apiKey;
    private RedLink.Analysis redlink;

    protected boolean available;

    @Override
    public void init() {
        redlink = RedLinkFactory.createAnalysisClient(apiKey);
        available = true;
        log.info("initialized redlink client with key '{}'", apiKey);
    }

    @Override
    public void destroy() {
        redlink = null;
        available = false;
    }

    @Override
    public Collection<Target> extract(String content) {
        if (StringUtils.isBlank(content)) {
            log.warn("empty/null content, so actually we skip the call");
            return Collections.emptyList();
        } else {
            try {
                final Enhancements enhancements = enhance(content);
                return enhancements
                        .getEntities()
                        .stream()
                                //TODO: find a better way to have unique identifiers for other later on
                        .map(entity -> entity.getUri())
                        .map(uri -> uri.substring(uri.lastIndexOf('/') + 1))
                        .map(name -> new Target().setEntity_name(name))
                        .collect(Collectors.toList());
            } catch (RuntimeException e) {
                log.error("Runtime error performing NER against Redlink: {}", e.getMessage(), e);
                return Collections.emptyList();
            }
        }
    }

    private Enhancements enhance(String content) {
        final AnalysisRequest request = AnalysisRequest.builder()
                .setContent(content)
                .setOutputFormat(AnalysisRequest.OutputFormat.TURTLE).build();
        if (redlink == null || available == false) {
            init();
        }
        return redlink.enhance(request);
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
