package io.redlink.ssix.pipeline.nlp.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.Analyzer;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.util.ReflectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

/**
 * Abstract implementation of a Analyzer REST Wrapper to easily allow new implementations
 *
 * @author Sergio Fernández
 */
public abstract class AnalyzerRestWrapper<T> implements External, Analyzer<T> {

    private static final Logger log = LoggerFactory.getLogger(AnalyzerRestWrapper.class);

    public abstract String getEndpoint();

    public abstract ResponseHandler<T> getResponseHandler(); //TODO: define a common data model

    protected boolean available;

    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Send a POST to the expected endpoint handled by the ResponseHandler provided
     *
     * @param content content to analyze
     * @return sentiment
     */
    @Override
    public T analyze(Content content) {
        if (Objects.isNull(content) || StringUtils.isBlank(content.getContent())) {
            log.warn("empty/null content, so we skip the call");
            final Class<?> type = ReflectionUtils.getGenericType(this);
            return ReflectionUtils.instantiate((Class<T>) type);
        } else {
            try (CloseableHttpClient client = createClient()) {
                final String endpoint = getEndpoint();
                final HttpPost post = new HttpPost(endpoint);
                post.setEntity(new StringEntity(mapper.writeValueAsString(content)));
                post.addHeader("content-type", "application/json");
                return client.execute(post, getResponseHandler());
            } catch (IOException e) {
                log.error("Error initializing analyzing with {}: {}", this.getClass().getCanonicalName(), e.getMessage(), e);
                log.debug("Content was: \n{}", content);
                final Class<?> type = ReflectionUtils.getGenericType(this);
                return ReflectionUtils.instantiate((Class<T>) type);
            }
        }
    }

    protected CloseableHttpClient createClient() {
        return HttpClients.createDefault();
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }
}
