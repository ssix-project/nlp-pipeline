package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Content;

import java.io.Serializable;

/**
 * Initial proposal for a very simple analyzer api
 *
 * @author Sergio Fernández
 */
public interface Analyzer<T> extends Serializable {

    T analyze(Content content);

    void init();

    void destroy();

}
