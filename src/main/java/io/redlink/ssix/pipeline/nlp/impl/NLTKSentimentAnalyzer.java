package io.redlink.ssix.pipeline.nlp.impl;

/**
 * Sentiment analyzer implementation relying on NLTK (Python)
 * via an ad-hoc RESTful interface
 *
 * @author sergio.fernandez@redlink.co
 */
public class NLTKSentimentAnalyzer extends SentimentMicroserviceAnalyzer {

    public NLTKSentimentAnalyzer() {
        super("NLTK");
    }

}
