package io.redlink.ssix.pipeline.nlp.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.RestResult;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.Translator;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * GeoFluent REST translator
 *
 * @author Sergio Fernández
 */
public class GeoFluentRestTranslator implements Translator, External {

    private static final Logger log = LoggerFactory.getLogger(GeoFluentRestTranslator.class);

    private String host;
    private int port;
    private boolean available;
    private ObjectMapper mapper;

    @Override
    public void init() {
        try(CloseableHttpClient client = createClient()) { //TODO: reuse client
            final String endpoint = getEndpoint();
            final HttpHead head = new HttpHead(endpoint);
            final CloseableHttpResponse response = client.execute(head);
            if (response.getStatusLine().getStatusCode() == 200) {
                log.info("Initialized GeoFluentRestTranslator client over '{}'", endpoint);
                setAvailable(true);
            } else {
                setAvailable(false);
                throw new IOException("GeoFluentRestTranslator server not found at " + endpoint);
            }
        } catch (IOException e) {
            log.error("Error initializing GeoFluentRestTranslator client: {}", e.getMessage());
            //throw new IllegalStateException(e);
        }
    }

    @Override
    public void destroy() {
        available = false;
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available = available;
    }

    protected CloseableHttpClient createClient() {
        return HttpClients.createDefault();
    }

    @Override
    public String translate(String content, String source, String target) {
        if (StringUtils.isBlank(content)) {
            log.warn("empty/null content, so we skip the call");
            return "";
        } else {
            try (CloseableHttpClient client = createClient()) {
                final String endpoint = getEndpoint(source, target);
                final HttpPost post = new HttpPost(endpoint);
                post.setEntity(new StringEntity(content, "UTF-8"));
                log.debug("Translating '{}' against '{}'", content, endpoint);
                return client.execute(post, getResponseHandler());
            } catch (IOException e) {
                log.error("Error initializing analyzing with {}: {}", this.getClass().getCanonicalName(), e.getMessage(), e);
                log.debug("Content was: \n{}", content);
                return null;
            }
        }
    }

    private ResponseHandler<String> getResponseHandler() {
        return response -> {
            final RestResult result = mapper.readValue(response.getEntity().getContent(), RestResult.class);
            return result.getTranslation().getText();
        };
    }

    //TODO: move up to External?
    private String getEndpoint() {
        return String.format("http://%s:%d/", host, port);
    }

    private String getEndpoint(String source, String target) {
        return String.format("http://%s:%d/?source=%s&target=%s", host, port, source, target);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

}
