package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.geofluent.GeoFluentClient;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Language Identifier relying on GeoFluent API
 *
 * @author Sergio Fernández
 */
public class LanguageIdentifierGeoFluent implements LanguageIdentifier {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentifierGeoFluent.class);

    @Autowired
    private GeoFluentClient geoFluentClient;

    @Override
    public String identifyLanguage(String content) {
        try {
            return geoFluentClient.detectLanguage(content);
        } catch (IOException | URISyntaxException e) {
            log.error("Error detecting language: {}", e.getMessage());
            return null;
        }
    }

    public void setGeoFluentClient(GeoFluentClient geoFluentClient) {
        this.geoFluentClient = geoFluentClient;
    }

}
