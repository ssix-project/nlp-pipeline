package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;

import java.util.Random;

/**
 * Dummy sentiment analyzer returning random sentiments
 * (just for pipeline testing purposes)
 *
 * @author sergio.fernandez@redlink.co
 */
public class RandomSentimentAnalyzer implements SentimentAnalyzer {

    private Random rand;

    @Override
    public void init() {
        this.rand =  new Random();
    }

    @Override
    public void destroy() {

    }

    @Override
    public Sentiment analyze(Content content) {
        return new Sentiment(Sentiment.MIN_VALUE + (Sentiment.MAX_VALUE - Sentiment.MIN_VALUE) * rand.nextDouble());
    }

}
