package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Content;

/**
 * Initial proposal for a very simple sentiment analyzer api
 *
 * @author Sergio Fernández
 */
public interface SentimentAnalyzer extends Analyzer<Sentiment> {

    Sentiment analyze(Content content);

    void init();

    void destroy();

}
