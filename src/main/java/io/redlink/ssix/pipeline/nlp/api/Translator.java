package io.redlink.ssix.pipeline.nlp.api;

import java.io.Serializable;

/**
 * Simple translator interface
 *
 * @author Sergio Fernández
 */
public interface Translator extends Serializable {

    void init();

    String translate(String content, String source, String target);

    //boolean supports(String sourceLanguage, String targetLanguage);

    void destroy();

}
