package io.redlink.ssix.pipeline.nlp.impl;

/**
 * Sentiment analyzer implementation relying on Passau's deep classifier
 * (implemented in Python) via an ad-hoc RESTful interface
 *
 * @author sergio.fernandez@redlink.co
 */
public class PassauSentimentAnalyzer extends SentimentMicroserviceAnalyzer {

    public PassauSentimentAnalyzer() {
        super("Passau");
    }

}
