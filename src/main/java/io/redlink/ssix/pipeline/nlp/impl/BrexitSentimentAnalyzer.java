package io.redlink.ssix.pipeline.nlp.impl;

/**
 * Sentiment analyzer implementation relying on the custom trained
 * Brexit classifier (Python) via a RESTful interface
 *
 * @author sergio.fernandez@redlink.co
 */
public class BrexitSentimentAnalyzer extends SentimentMicroserviceAnalyzer {

    public BrexitSentimentAnalyzer() {
        super("Brexit");
    }

}
