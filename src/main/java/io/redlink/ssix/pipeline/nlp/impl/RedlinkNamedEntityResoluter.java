package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.sdk.RedLink;
import io.redlink.sdk.RedLinkFactory;
import io.redlink.sdk.impl.analysis.AnalysisRequest;
import io.redlink.sdk.impl.analysis.model.Enhancements;
import io.redlink.sdk.impl.analysis.model.EntityAnnotation;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.NamedEntityResoluter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Named-Entity Resolution and Linking relying on the Redlink Platform
 *
 * @author Alfonso Noriega Meneses
 */
public class RedlinkNamedEntityResoluter implements NamedEntityResoluter, External {

    private static final Logger log = LoggerFactory.getLogger(RedlinkNamedEntityResoluter.class);

    private String apiKey;
    private RedLink.Analysis redlink;

    protected boolean available;

    @Override
    public void init() {
        redlink = RedLinkFactory.createAnalysisClient(apiKey);
        available = true;
        log.info("initialized redlink client with key '{}'", apiKey);
    }

    @Override
    public void destroy() {
        redlink = null;
        available = false;
    }

    @Override
    public Collection<Target> analyze(Collection<Target> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            log.warn("empty/null content, so actually we skip the call");
            return Collections.emptyList();
        } else {
            try {

                final Collection<Target> resolvedEntities = entityResolution(entities);
                final String entityList = resolvedEntities.stream()
                        .map(t -> t.getEntity_name())
                        .collect( Collectors.joining( "," ) );
                final Enhancements enhancements = enhance(entityList);

                enhancements.getEnhancements().stream()
                        .filter(a -> EntityAnnotation.class.isAssignableFrom(a.getClass()))
                        .forEach(a -> {
                            final Target entity = (Target) CollectionUtils.find(resolvedEntities, o -> {
                                if (Target.class.isAssignableFrom(o.getClass())) {

                                    return ((Target) o).getEntity_name().contains(((EntityAnnotation) a).getEntityLabel());
                                }
                                return false;
                            });
                            entity.setUri(((EntityAnnotation) a).getEntityReference().getUri());
                        });

                return resolvedEntities;

            } catch (RuntimeException e) {
                log.error("Runtime error performing NEL against Redlink: {}", e.getMessage(), e);
                return Collections.emptyList();
            }
        }
    }

    private Enhancements enhance(String content) {
        final AnalysisRequest request = AnalysisRequest.builder()
                .setContent(content)
                .setOutputFormat(AnalysisRequest.OutputFormat.TURTLE).build();
        if (redlink == null || available == false) {
            init();
        }
        return redlink.enhance(request);
    }

    private Collection<Target> entityResolution(Collection<Target> entities) {
        final List<Target> mergedEntities = entities.stream()
                .filter( e -> StringUtils.isNotEmpty(e.getEntity_id()))//Exclude those entities not coming from the Entity DB
                .collect(Collectors.collectingAndThen(
                        Collectors.groupingBy(Target::uniqueFunction, Collectors.collectingAndThen(
                                Collectors.reducing((a,b) -> targetMergeFunction(a,b)), Optional::get)),
                        m -> new ArrayList<>(m.values())));

        return mergedEntities;
    }

    private Target targetMergeFunction(Target a, Target b) {
        if (StringUtils.isEmpty(a.getParent_id())){
            a.addAnnotation(b.getAnnotations());
            return a;
        } else {
            b.addAnnotation(a.getAnnotations());
            return b;
        }
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
