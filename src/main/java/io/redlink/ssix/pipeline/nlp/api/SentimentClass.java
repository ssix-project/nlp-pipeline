package io.redlink.ssix.pipeline.nlp.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sentiment Class
 *
 * @author Sergio Fernández
 * @see <a href="https://jira.ssix-project.eu/browse/PIPELINE-55">PIPELINE-55</a>
 */
public enum SentimentClass {

    StrongNegative("Strong Negative", -1000, -700),
    MediumNegative("Medium Negative", -700, -400),
    WeakNegative("Weak Negative", -400, -100),
    Neutral("Neutral", -100, 100),
    WeakPositive("Weak Positive", 100, 400),
    MediumPositive("Medium Positive", 400, 700),
    StrongPositive("Strong Positive", 700, 1000),
    Irrelevant("Irrelevant");

    private final String label;
    private final Integer from;
    private final Integer to;

    SentimentClass(String label, Integer from, Integer to) {
        this.label = label;
        this.from = from;
        this.to = to;
    }

    SentimentClass(String label, Double from, Double to) {
        this(label, new Double(from * 1000).intValue(), new Double(to * 1000).intValue());
    }

    SentimentClass(String label) {
        this(label, (Integer)null, (Integer)null);
    }

    public String label() {
        return label;
    }

    public static SentimentClass fromSentiment(Sentiment sentiment) {
        return fromValue(sentiment.value());
    }

    private static final Logger log = LoggerFactory.getLogger(SentimentClass.class);


    public static SentimentClass fromValue(double value) {
        if (-1000 <= value && value < -700) {
            return SentimentClass.StrongNegative;
        } else if (-700 <= value && value < -400) {
            return SentimentClass.MediumNegative;
        } if (-400 <= value && value < -100) {
            return SentimentClass.WeakNegative;
        } if (-100 <= value && value <= 100) {
            return SentimentClass.Neutral;
        } if (100 < value && value <= 400) {
            return SentimentClass.WeakPositive;
        } if (400 < value && value <= 700) {
            return SentimentClass.MediumPositive;
        } if (700 < value && value <= 1000) {
            return SentimentClass.StrongPositive;
        } else {
            //should not happen...
            log.warn("sentiment value {} can't be classified");
            return SentimentClass.Irrelevant;
        }
    }

}
