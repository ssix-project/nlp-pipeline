package io.redlink.ssix.pipeline.nlp.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.pipeline.model.RestResult;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Sentiment analyzer implementation relying on Financial Classifier microservice
 *
 * @author sergio.fernandez@redlink.co
 */
public class FinancialSentimentAnalyzer extends AnalyzerRestWrapper<Sentiment> implements SentimentAnalyzer {

    private static final Logger log = LoggerFactory.getLogger(FinancialSentimentAnalyzer.class);

    private final String name;
    private String host;
    private int port;
    private ObjectMapper mapper;

    public FinancialSentimentAnalyzer() {
        this.name = "Financial";
    }

    @Override
    public void init() {
        try (CloseableHttpClient client = createClient()) { //TODO: reuse client
            final String endpoint = getEndpoint();
            final HttpHead head = new HttpHead(endpoint);
            final CloseableHttpResponse response = client.execute(head);
            if (response.getStatusLine().getStatusCode() == 200) {
                log.info("Initialized {} client over '{}'", name, endpoint);
                setAvailable(true);
            } else {
                setAvailable(false);
                throw new IOException(name + " server not found at " + endpoint);
            }
        } catch (IOException e) {
            log.error("Error initializing {} client: {}", name, e.getMessage());
            setAvailable(false);
            //throw new RuntimeException(e);
        }
    }

    @Override
    public void destroy() {
        setAvailable(false);
    }

    @Override
    public String getEndpoint() {
        return String.format("http://%s:%d/", host, port);
    }

    @Override
    public ResponseHandler<Sentiment> getResponseHandler() {
        return response -> {
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode >= 200 && statusCode < 300) {
                final RestResult results = mapper.readValue(response.getEntity().getContent(), RestResult.class);
                final Sentiment sentiment = new Sentiment(results.getSentiment());
                results.getTargets()
                        .forEach((l, t) -> sentiment.addTarget(l, new Sentiment(t.getSentiment())));
                return sentiment;
            } else {
                final Map error = mapper.readValue(response.getEntity().getContent(), Map.class);
                log.error("Analyzer responded with {} status code: {}", statusCode, error.get("error"));
                return new Sentiment(); //TODO: continue or skip
            }
        };
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

}
