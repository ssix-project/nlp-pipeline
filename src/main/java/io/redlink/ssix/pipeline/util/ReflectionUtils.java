package io.redlink.ssix.pipeline.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Some basic reflection utils
 *
 * @author Sergio Fernández
 */
public class ReflectionUtils {

    private static final Logger log = LoggerFactory.getLogger(ReflectionUtils.class);

    /**
     * Dirty trick to find the generic type at runtime
     *
     * @param o target object
     * @return
     */
    public static Class<?> getGenericType(Object o) {
        Type superClass = o.getClass().getGenericSuperclass();
        while (!(superClass instanceof ParameterizedType)) {
            superClass = ((Class)superClass).getGenericSuperclass();
        }
        return (Class) ((ParameterizedType)superClass).getActualTypeArguments()[0];
    }

    public static <T> T instantiate(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("Error creating {} instance by reflection", clazz.getCanonicalName());
            return null;
        }
    }

}
