package io.redlink.ssix.pipeline.util;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.beans.factory.support.AbstractBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Helper with some Spring utils
 *
 * @author Sergio Fernández
 */
public class SpringUtils extends PropertyPlaceholderConfigurer {

    public static Map<String,Object> getSpringProperties(String file) throws IOException {
        final Properties properties = PropertiesLoaderUtils.loadAllProperties(file);
        final Map<String,Object> map = new HashMap<>();
        for (final String name: properties.stringPropertyNames()) {
            map.put(name, properties.getProperty(name));
        }
        return map;
    }

    public static Map<String, String> getSpringProperties(ApplicationContext context) throws IOException {
        final Map<String, String> props = new HashMap<>();
        for (String name: getSpringProperties("pipeline.yaml").keySet()) {
            final String prop = String.format("${%s}", name);
            final String value = ((AbstractBeanFactory) context.getAutowireCapableBeanFactory()).resolveEmbeddedValue(prop);
            props.put(name, value);
        }
        return props;
    }

}
