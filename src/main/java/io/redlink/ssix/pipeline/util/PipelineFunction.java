/*
 * Copyright (c) 2017 Redlink GmbH.
 */
package io.redlink.ssix.pipeline.util;

/**
 * Created on 21.12.17.
 */
public enum PipelineFunction {
    NER,NEL,SENTIMENT,EVENT,TRANSLATE,LANG_IDENT;
}
