package io.redlink.ssix.pipeline;

import io.redlink.ssix.pipeline.functions.*;
import io.redlink.ssix.pipeline.model.Content;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.MapPropertySource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic Spark-based streaming pipeline
 *
 * @author alfonso.noriega@redlink.co
 * @author sergio.fernandez@redlink.io
 */
public class SSIXAnalysisSparkPipeline {

    private static final Logger log = LoggerFactory.getLogger(SSIXAnalysisSparkPipeline.class);

    private String sparkAppName;
    private int sparkBatchSpan;
    private String zookeeperQuorum;
    private String kafkaTopic;
    private String kafkaConsumerGroup;

    private JavaStreamingContext context;
    private JsonParseFunction jsonParseFunction;
    private LanguageIdentificationFunction languageIdentificationFunction;
    private TranslateFunction translateFunction;
    private NERFunction nerFunction;
    private NELFunction nelFunction;

    private EventRecognizerFunction eventRecognizerFunction;

    private SentimentFunction sentimentFunction;
    private TimeFilterFunction timeFilter;
    private DataCollector dataCollector;

    public synchronized void exec() {
        final SparkConf conf = new SparkConf().setAppName(sparkAppName);
        final JavaSparkContext ctx = JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(conf));
        context = new JavaStreamingContext(ctx, Durations.seconds(sparkBatchSpan));

        log.info("SSIX streaming context is at state {}", context.getState());

        // 1. prepare the stream reader from Kafka
        log.debug("Connecting to zookeeper at '{}'...", zookeeperQuorum);
        final Map<String, Integer> topicMap = new HashMap<String, Integer>() {{
            put(kafkaTopic, 1);
        }};
        final JavaPairReceiverInputDStream<String, String> kafkaStream =
                KafkaUtils.createStream(context, zookeeperQuorum, kafkaConsumerGroup, topicMap);

        final Broadcast<DataCollector> broadcastedDataCollector = context.sparkContext().broadcast(dataCollector);

        // 2. each tweet has to go through all functions in the pipeline
        final JavaDStream<Content> contents = kafkaStream
                .map(jsonParseFunction)
                //.filter(timeFilter)
                .map(languageIdentificationFunction)
                //TODO: only translate if necessary
                .map(translateFunction)
                //TODO: paralyze
                .map(nerFunction)
                .map(nelFunction)
                .map(eventRecognizerFunction)
                .map(sentimentFunction);

        // 3. post-process the results (store and push to ui)
        contents.foreachRDD(rdd -> {
            rdd.foreachPartition(iter -> {
                while (iter.hasNext()) {
                    Content content = iter.next();
                    content.getMetadata().setEnd(new Date().getTime());
                    broadcastedDataCollector.getValue().put(content);
                    //dataCollectorFunction.accept(content); //not necessary because was already broadcast?
                }
            });
        });

        context.start();              // Start the computation
        log.info("SSIX streaming context is at state {}", context.getState());
        context.awaitTermination();   // Wait for the computation to terminate
    }

    public static void main(String[] args) throws Exception {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});

        // PIPELINE-253
        if (args.length > 0) {
            final Map<String, Object> propertiesSource = new HashMap<>();
            for (String arg: args) {
                if (arg.startsWith("-D")) {
                    final String[] prop = arg.substring(2).split("=");
                    if (prop.length == 2) {
                        propertiesSource.put(prop[0], prop[1]);
                    } else {
                        log.warn("Ignored invalid argument '{}'", arg);
                    }
                } else {
                    log.warn("Ignored argument '{}'", arg);
                }
            }

            if (propertiesSource.size() > 0) {
                log.debug("Refreshing Pipeline context with {} new properties from cli...", propertiesSource.size());
                context.getEnvironment().getPropertySources().addLast(new MapPropertySource("cli", propertiesSource));
                context.refresh();
            }
        }

        final SSIXAnalysisSparkPipeline pipeline = (SSIXAnalysisSparkPipeline)context.getBean("sparkPipeline");
        pipeline.exec();
    }

    public void shutdown() {
        if (context != null) {
            log.info("Shutting-down SSIX streaming context...");
            context = null;
        } else {
            log.warn("Received shutdown hook, but SSIX streaming context is not active");
        }
    }

    public void setSparkAppName(String sparkAppName) {
        this.sparkAppName = sparkAppName;
    }

    public void setSparkBatchSpan(int sparkBatchSpan) {
        this.sparkBatchSpan = sparkBatchSpan;
    }

    public void setKafkaConsumerGroup(String kafkaConsumerGroup) {
        this.kafkaConsumerGroup = kafkaConsumerGroup;
    }

    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }

    public void setZookeeperQuorum(String zookeeperQuorum) {
        this.zookeeperQuorum = zookeeperQuorum;
    }

    public void setTimeFilter(TimeFilterFunction timeFilter) {
        this.timeFilter = timeFilter;
    }

    public void setDataCollector(DataCollector dataCollector) {
        this.dataCollector = dataCollector;
    }

    public void setJsonParseFunction(JsonParseFunction jsonParseFunction) {
        this.jsonParseFunction = jsonParseFunction;
    }

    public void setLanguageIdentificationFunction(LanguageIdentificationFunction languageIdentificationFunction) {
        this.languageIdentificationFunction = languageIdentificationFunction;
    }

    public void setTranslateFunction(TranslateFunction translateFunction) {
        this.translateFunction = translateFunction;
    }

    public void setNerFunction(NERFunction nerFunction) {
        this.nerFunction = nerFunction;
    }

    public void setNelFunction(NELFunction nelFunction) {
        this.nelFunction = nelFunction;
    }

    public void setEventRecognizerFunction(EventRecognizerFunction eventRecognizerFunction) {
        this.eventRecognizerFunction = eventRecognizerFunction;
    }

    public void setSentimentFunction(SentimentFunction sentimentFunction) {
        this.sentimentFunction = sentimentFunction;
    }

}
